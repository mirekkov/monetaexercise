package com.unicorn.hatchery.springexercise.exercise.controller;

import com.unicorn.hatchery.springexercise.exercise.service.CalculatorService;
import com.unicorn.hatchery.springexercise.exercise.service.CalculatorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "calculate")
public class CalculatorController {

    @Autowired
    private CalculatorService service;

    @GetMapping
    public long calculateNumber(@RequestParam("number") long number) {
        return service.calculate(number);
    }
}
