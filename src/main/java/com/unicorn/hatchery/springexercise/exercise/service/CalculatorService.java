package com.unicorn.hatchery.springexercise.exercise.service;

public interface CalculatorService {

    String moveToRight(String number);
    String multiplyByTwo(String number);
    String deleteSeven(String number);
    long calculateEven(String number);
    long calculate(long number);
}
