package com.unicorn.hatchery.springexercise.exercise.service;

import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculatorService{

    /**
     * Numbers which are less or equal to 3 are moved one position
     * to the right
     * @param number input string number
     * @return string
     */
    public String moveToRight(String number) {
        char[] array = number.toCharArray();
        char pom;
        for (int i = array.length -1; i > 0; i--) {
            int actual = Character.getNumericValue(array[i]);
            int actualMinusOne = Character.getNumericValue(array[i-1]);
            if (actual > 3 && actualMinusOne <= 3) {
                pom = array[i];
                array[i] = array[i-1];
                array[i-1] = pom;
            }
        }
        return String.valueOf(array);
    }

    /**
     * Numbers 8 and 9 in the sequence are multiplied by 2
     * @param number input string number
     * @return string
     */
    public String multiplyByTwo(String number) {
        number = number.replaceAll("8", "16");
        number = number.replaceAll("9","18");
        return number;
    }

    /**
     * Number 7 in the sequence is deleted
     * @param number input string number
     * @return string
     */
    public String deleteSeven(String number) {
        return number.replaceAll("7", "");
    }

    /**
     * Method counts even numbers in the sequence.
     * Input number is divided by the count of even numbers.     *
     * @param number input string number
     * @return long number
     */
    public long calculateEven(String number) {
        char[] array = number.toCharArray();
        int evenNumbers = 0;

        for (int i=0; i < number.length(); i++) {
            int actual = Character.getNumericValue(array[i]);
            if (actual % 2 == 0) {
                evenNumbers++;
            }
        }
        long longNumber = Long.valueOf(number);
        long result = longNumber / evenNumbers;
        return result;
    }

    /**
     * Methods (moveToRight, multiplyByTwo, deleteSeven, calculateEven)
     * are applied to input number
     * @param number input long number
     * @return long number
     */
    public long calculate(long number) {
        String numberStr = String.valueOf(number);
        String one = moveToRight(numberStr);
        String two = multiplyByTwo(one);
        String three = deleteSeven(two);
        return calculateEven(three);
    }

}
