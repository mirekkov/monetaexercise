package com.unicorn.hatchery.springexercise.exercise.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = CalculatorServiceImpl.class)
public class CalculatorServiceImplTest {

    @Autowired
    CalculatorService service;

    @Test
    public void moveToRight() {
        String string1 = "43256791";
        String result = service.moveToRight(string1);
        Assert.assertEquals("45326791", result);
    }

    @Test
    public void multiplyByTwo() {
        String string1 = "45326791";
        String result = service.multiplyByTwo(string1);
        Assert.assertEquals("453267181", result);
    }

    @Test
    public void deleteSeven() {
        String string1 = "453267181";
        String result = service.deleteSeven(string1);
        Assert.assertEquals("45326181", result);
    }

    @Test
    public void calculateEven() {
        String string1 = "45326181";
        long result = service.calculateEven(string1);
        Assert.assertEquals(11331545 , result);
    }

}